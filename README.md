![build status](https://gitlab.com/apiazza134/problemi-qcd/badges/master/pipeline.svg)

# Problemi di Cromodinamica
Scansioni delle soluzioni dei problemi assegnati durante il corso di <ins>*"Cromo-Dinamica Quantistica"*</ins>

Il PDF con tutte le soluzioni incollate e indicizzate si può scaricare [qui](https://gitlab.com/apiazza134/problemi-qcd/-/jobs/artifacts/master/raw/problemi-qcd.pdf?job=compile_pdf).
